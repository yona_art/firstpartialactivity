<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
    <title>All Todo</title>
</head>

<body>
    <table>
        <th>ID</th>
        <th>List</th>
        <th>IS_Done</th>
            <c:forEach items="${list}" var="toDo">
                <tr>
                    <td>
                    ${toDo.id}
                    </td>

                   <td>${toDo.list}</td>
                
                
                <c:if test="${toDo.isDone == 0}">
                
                <td><a href="UpdateList?id=${toDo.id}&done=1">Done</a></td>
                </tr>
            	</c:if>
                <c:if test="${toDo.isDone == 1}">

                <td><a href="UpdateList?id=${toDo.id}&done=0">Is Not Done</a></td>
                </tr>
            	</c:if>
            </c:forEach>

    </table>
</body>

</html>