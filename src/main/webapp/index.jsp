<%@
   page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="ISO-8859-1"
%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="ISO-8859-1">
    <title>ToDoList</title>
</head>

<body>
    <ul>
		<li>
		<a href="CreateTodo">Create Todo task</a>
		</li>
        <li>
            <a href="GetList">Go To My To Do List</a>
        </li>
        <li>
            <a href="GetList?done=0">Get Not Done List</a>
        </li>
        <li>
            <a href="GetList?done=1">Get Done List</a>
        </li>
    </ul>
</body>

</html>