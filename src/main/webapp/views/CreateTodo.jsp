<!DOCTYPE html>
<html>
<head>
    <title>Create To do</title>
</head>
<body>

    <form action="CreateList" method="post" id="form">
        <label for="list"> New Task</label>
        <input type="text" name="list">
        <label for="done"> Is Done?</label>
        <input type="checkbox" name="done">
        <input type="submit" value="Create Task">
    </form>
</body>
</html>
