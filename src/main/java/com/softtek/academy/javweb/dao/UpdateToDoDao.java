package com.softtek.academy.javweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.softtek.academy.javweb.model.DBConnection;

/**
 * UpdateToDoDao
 */
public class UpdateToDoDao {

    public Boolean UpdateList(int id, int done) {
        try {
        	Connection db = DBConnection.getInstance().getConnection();
            PreparedStatement ps = db.prepareStatement("UPDATE TO_DO_LIST SET is_done = ? WHERE id = ?");
            ps.setInt(1, done);
            ps.setInt(2, id);
            int count = ps.executeUpdate();
            return count > 0 ? true : false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}