package com.softtek.academy.javweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.softtek.academy.javweb.model.DBConnection;

public class CreateTodoDao {
	   public int createTodoTaks(String list, int done) {
	        try  {
	        	Connection db = DBConnection.getInstance().getConnection();
	        	PreparedStatement ps = db.prepareStatement("INSERT INTO TO_DO_LIST (list, is_done) values (?, ?)",
	                    Statement.RETURN_GENERATED_KEYS);
	            ps.setString(1, list);
	            ps.setInt(2, done);
	            ps.executeUpdate();
	            ResultSet rs = ps.getGeneratedKeys();
	            if (rs.next()) {
	                return  rs.getInt(1);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	            return 0;
	        }
	        return 0;
	    }

}
