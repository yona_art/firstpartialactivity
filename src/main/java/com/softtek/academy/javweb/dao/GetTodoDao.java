package com.softtek.academy.javweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javweb.model.Connections;
import com.softtek.academy.javweb.model.DBConnection;
import com.softtek.academy.javweb.model.ToDoListBean;

public class GetTodoDao {

    public List<ToDoListBean> getList(int done, Boolean all) {
        List<ToDoListBean> list = new ArrayList<ToDoListBean>();
        try  {
        	Connection db = DBConnection.getInstance().getConnection();
        	ResultSet rs = null;
            if (all) {
                rs = db.prepareStatement("SELECT * FROM TO_DO_LIST").executeQuery();
            } else {
                PreparedStatement ps = db.prepareStatement("SELECT * FROM TO_DO_LIST WHERE is_done = ?");
                ps.setInt(1, done);
                rs = ps.executeQuery();
            }
            while (rs.next()) {
                list.add(new ToDoListBean(rs.getInt(1), rs.getString(2), rs.getInt(3)));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
