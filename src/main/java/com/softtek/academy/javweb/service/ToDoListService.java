
package com.softtek.academy.javweb.service;

import java.util.List;

import com.softtek.academy.javweb.dao.CreateTodoDao;
import com.softtek.academy.javweb.dao.GetTodoDao;
import com.softtek.academy.javweb.dao.UpdateToDoDao;
import com.softtek.academy.javweb.model.ToDoListBean;

/**
 * ToDoList.service
 */
public class ToDoListService {
    
    public List<ToDoListBean> getAllList(int done, Boolean all){
        GetTodoDao GT = new GetTodoDao();
        return GT.getList(done, all);
    }

    public Boolean UpdateList(int id, int done){
        UpdateToDoDao UPTDao = new UpdateToDoDao();
        return UPTDao.UpdateList(id, done);
    }
    
    public int createTodo(String list, int done){
    	CreateTodoDao CTD = new CreateTodoDao();
        return CTD.createTodoTaks(list,done);
    }

    
}