package com.softtek.academy.javweb.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * DBConection
 */
public class DBConnection {
	private static DBConnection instance;
	private Connection connection;
	private String url = "jdbc:mysql://localhost:3306/test?serverTimezone=UTC#";
	private String username = "root";
	private String password = "1234";

	private DBConnection() throws SQLException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			this.connection = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException ex) {
			System.out.println("Database Connection Creation Failed : " + ex.getMessage());
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public static DBConnection getInstance() throws SQLException {
		if (instance == null) {
			instance = new DBConnection();
		} else {
			return instance;
		}
		return instance;

	}
}