package com.softtek.academy.javweb.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javweb.model.ToDoListBean;
import com.softtek.academy.javweb.service.ToDoListService;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

/**
 * ControllerServlet
 */
public class UpdateListController extends HttpServlet {

    public UpdateListController() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ToDoListService todoListService = new ToDoListService();
        Boolean updated = todoListService.UpdateList(Integer.parseInt(request.getParameter("id")),
                Integer.parseInt(request.getParameter("done")));
        if(updated){
            List<ToDoListBean> list = todoListService.getAllList(0 ,true);
            RequestDispatcher rd = request.getRequestDispatcher("/views/TodoList.jsp");
            request.setAttribute("list", list);
            rd.forward(request, response);
        }else {
            RequestDispatcher rd = request.getRequestDispatcher("/views/Error.jsp");
            rd.forward(request, response);
        }
    }

}