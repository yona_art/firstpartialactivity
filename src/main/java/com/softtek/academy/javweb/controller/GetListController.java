package com.softtek.academy.javweb.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javweb.model.ToDoListBean;
import com.softtek.academy.javweb.service.ToDoListService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

/**
 * ControllerServlet
 */
public class GetListController extends HttpServlet {

    public GetListController() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<ToDoListBean> list = new ArrayList<ToDoListBean>();
        ToDoListService todoListService = new ToDoListService();
        String done = request.getParameter("done");
        if (done != null && !done.isEmpty()) { 
            list = todoListService.getAllList(Integer.parseInt(done), false);
        } else {
            list = todoListService.getAllList(0, true);
        }
        RequestDispatcher rd = request.getRequestDispatcher("/views/TodoList.jsp");
        request.setAttribute("list", list);
        rd.forward(request, response);
    }

}