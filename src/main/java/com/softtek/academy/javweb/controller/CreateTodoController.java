package com.softtek.academy.javweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javweb.model.ToDoListBean;
import com.softtek.academy.javweb.service.ToDoListService;

public class CreateTodoController extends HttpServlet {
	
    public CreateTodoController() {
        super();

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String list = request.getParameter("list");
        Boolean is_done = Boolean.valueOf(request.getParameter("done"));
        ToDoListService todoService = new ToDoListService();
        int created = todoService.createTodo(list, is_done  == false ? 0 : 1);
        ToDoListService todoListService = new ToDoListService();
        if(created == 0){
            List<ToDoListBean> listItem = todoListService.getAllList(0 ,true);
            RequestDispatcher rd = request.getRequestDispatcher("/views/TodoList.jsp");
            request.setAttribute("list", listItem);
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("/views/Error.jsp");
            rd.forward(request, response);
        }
    }

}
